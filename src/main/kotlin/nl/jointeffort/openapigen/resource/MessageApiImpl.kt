package nl.jointeffort.openapigen.resource

import nl.jointeffort.service.messageposter.generated.api.MessagesApi
import nl.jointeffort.service.messageposter.generated.model.Message
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RestController
import java.net.URI
import java.util.*

@RestController
class MessageApiImpl : MessagesApi {

    override fun createMessage(xRequestID: String, message: Message): ResponseEntity<Unit> {
        return ResponseEntity.created(URI.create(UUID.randomUUID().toString())).build()
    }
}
