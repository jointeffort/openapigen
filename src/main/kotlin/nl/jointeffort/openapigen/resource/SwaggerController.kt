package nl.jointeffort.openapigen.resource

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ResourceLoader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController

//@RestController
class SwaggerController {

//    @Autowired
    private lateinit var resourceLoader: ResourceLoader

    @RequestMapping(
        value = ["/v3/swagger"],
        method = [RequestMethod.GET],
        produces = ["application/json"]
    )
//    @ResponseBody
    fun getText(): ByteArray? {
        return resourceLoader.getResource("classpath:/openapi/MessagePoster_v1_0_0.json").inputStream.readBytes()
    }

}
