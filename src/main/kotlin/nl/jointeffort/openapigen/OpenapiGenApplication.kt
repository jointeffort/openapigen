package nl.jointeffort.openapigen

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class OpenapiGenApplication

fun main(args: Array<String>) {
	runApplication<OpenapiGenApplication>(*args)
}
